<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\DropAccount\Tests\Activity;

use ChristophWurst\Nextcloud\Testing\TestCase;
use DateTime;
use InvalidArgumentException;
use OCA\DropAccount\Activity\Provider;
use OCA\DropAccount\AppInfo\Application;
use OCP\Activity\IEvent;
use OCP\Activity\IEventMerger;
use OCP\IL10N;
use OCP\IURLGenerator;
use OCP\L10N\IFactory;
use PHPUnit\Framework\MockObject\MockObject;

class ProviderTest extends TestCase {
	/**
	 * @var IURLGenerator|MockObject
	 */
	private $urlGenerator;
	/**
	 * @var IEventMerger|MockObject
	 */
	private $eventMerger;
	private Provider $provider;

	public function setUp(): void {
		parent::setUp();
		$languageFactory = $this->createMock(IFactory::class);
		$this->urlGenerator = $this->createMock(IURLGenerator::class);
		$this->eventMerger = $this->createMock(IEventMerger::class);
		$l10n = $this->createMock(IL10N::class);
		$l10n
			->method('t')
			->willReturnCallback(function ($string, $args) {
				return 'TRANSLATED: ' . vsprintf($string, $args);
			});
		$l10n->method('l')->willReturnCallback(function (string $type, int $value, array $options) {
			return (new DateTime())->setTimestamp($value)->format('d M Y');
		});
		$languageFactory->expects($this->any())->method('get')->with(Application::APP_NAME, $this->anything())->willReturn($l10n);
		$this->provider = new Provider($languageFactory, $this->urlGenerator, $this->eventMerger);
	}

	public function testProvideActivityFromOtherApp(): void {
		$event = $this->createMock(IEvent::class);
		$event->expects($this->once())->method('getApp')->willReturn('otherapp');
		$this->expectException('InvalidArgumentException');
		$this->provider->parse('en', $event);
	}

	public function testProvideActivityFromOtherType(): void {
		$event = $this->createMock(IEvent::class);
		$event->expects($this->once())->method('getApp')->willReturn(Application::APP_NAME);
		$event->expects($this->once())->method('getType')->willReturn('other_type');
		$this->expectException('InvalidArgumentException');
		$this->provider->parse('en', $event);
	}

	public function testParseWithNoSubjectParameters(): void {
		$event = $this->setupEvent();
		$event->expects($this->once())->method('getSubjectParameters')->willReturn(['email' => 'olduser@email.com']);

		$this->expectException(InvalidArgumentException::class);
		$this->expectExceptionMessage('Missing required username in parameters');

		$this->provider->parse('en', $event);
	}

	/**
	 * @dataProvider dataForTestParse
	 */
	public function testParse(string $subject, array $parameters, string $output): void {
		$event = $this->setupEvent();
		$event->expects($this->once())->method('getSubject')->willReturn($subject);
		$event->expects($this->once())->method('getSubjectParameters')->willReturn($parameters);
		$event->expects($this->once())->method('setParsedSubject')->with($output);
		$finalEvent = $this->createMock(IEvent::class);
		$this->eventMerger->expects($this->once())->method('mergeEvents')->with('username', $event, null)->willReturn($finalEvent);

		$this->provider->parse('en', $event);
	}

	public function dataForTestParse(): array {
		return [
			[
				'account_self_deletion', ['username' => 'bye'], 'TRANSLATED: bye deleted their account',
			],
			[
				'account_self_deletion', ['username' => 'bye', 'name' => 'Dummy Account'], 'TRANSLATED: Dummy Account (bye) deleted their account'
			],
			[
				'account_self_deletion', ['email' => 'bye@me.com', 'name' => 'Dummy Account'], 'TRANSLATED: Dummy Account (bye@me.com) deleted their account'
			],
			[
				'account_self_deletion', ['email' => 'bye@me.com', 'name' => 'Dummy Account', 'username' => 'bye'], 'TRANSLATED: Dummy Account (bye - bye@me.com) deleted their account'
			],
			[
				'account_scheduled_self_deletion', ['username' => 'bye', 'expiration' => (new \DateTime('2016-06-02'))->getTimestamp()], 'TRANSLATED: bye requested to delete their account. It has been disabled and will automatically be removed on 02 Jun 2016',
			],
			[
				'account_scheduled_self_deletion', ['username' => 'bye', 'name' => 'Dummy Account', 'expiration' => (new \DateTime('2016-06-02'))->getTimestamp()], 'TRANSLATED: Dummy Account (bye) requested to delete their account. It has been disabled and will automatically be removed on 02 Jun 2016',
			],
			[
				'account_scheduled_self_deletion', ['email' => 'bye@me.com', 'name' => 'Dummy Account', 'expiration' => (new \DateTime('2016-06-02'))->getTimestamp()], 'TRANSLATED: Dummy Account (bye@me.com) requested to delete their account. It has been disabled and will automatically be removed on 02 Jun 2016',
			],
			[
				'account_scheduled_self_deletion', ['email' => 'bye@me.com', 'name' => 'Dummy Account', 'username' => 'bye', 'expiration' => (new \DateTime('2016-06-02'))->getTimestamp()], 'TRANSLATED: Dummy Account (bye - bye@me.com) requested to delete their account. It has been disabled and will automatically be removed on 02 Jun 2016',
			]
		];
	}

	private function setupEvent() {
		$event = $this->createMock(IEvent::class);
		$event->expects($this->once())->method('getApp')->willReturn(Application::APP_NAME);
		$event->expects($this->once())->method('getType')->willReturn('account_deletion');

		$this->urlGenerator->expects($this->once())->method('imagePath')->with('core', 'actions/delete.svg')->willReturn('smth.png');
		return $event;
	}
}
