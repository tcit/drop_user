<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\DropAccount\Tests\Service;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\Service\ActivityService;
use OCP\Activity\IEvent;
use OCP\Activity\IManager;
use OCP\IGroup;
use OCP\IGroupManager;
use OCP\IUser;
use PHPUnit\Framework\MockObject\MockObject;

class ActivityServiceTest extends TestCase {

	/**
	 * @var IManager|MockObject
	 */
	private $activityManager;

	/** @var IUser|(IUser&MockObject)|MockObject */
	private $user;

	private ActivityService $activityService;

	public function setUp(): void {
		parent::setUp();
		$this->activityManager = $this->createMock(IManager::class);
		$groupManager = $this->createMock(IGroupManager::class);
		$adminGroup = $this->createMock(IGroup::class);
		$groupManager->expects($this->once())->method('get')->with('admin')->willReturn($adminGroup);
		$this->user = $this->createMock(IUser::class);
		$this->user->method('getUID')->willReturn('someuid');
		$this->user->method('getDisplayName')->willReturn(null);
		$this->user->method('getEMailAddress')->willReturn(null);
		$admin = $this->createMock(IUser::class);
		$admin->method('getUID')->willReturn('someadminuid');
		$adminGroup->expects($this->once())->method('getUsers')->willReturn([$admin]);

		$this->activityService = new ActivityService($this->activityManager, $groupManager);
	}

	public function dataProviderForTestCreateAdminActivities(): array {
		return [
			[[]],
			[['subject' => 'account_scheduled_self_deletion', 'subject_params' => ['expiration' => new \DateTime()]]],
			[['subject' => 'account_re_enabled']]
		];
	}

	/**
	 * @dataProvider dataProviderForTestCreateAdminActivities
	 */
	public function testCreateAdminActivities(array $options) {
		$event = $this->getActivityEventMock($options['subject'] ?? 'account_self_deletion', array_merge($options['subject_params'] ?? [], ['username' => 'someuid', 'name' => null, 'email' => null]));
		$this->activityService->createAdminActivities($this->user, $options);
		$this->activityManager->expects($this->once())->method('publish')->with($event);
		$this->activityService->sendActivities();
	}

	private function getActivityEventMock(string $subject, array $options) {
		$event = $this->createMock(IEvent::class);
		$this->activityManager->expects($this->once())->method('generateEvent')->with()->willReturn($event);
		$event->expects($this->once())
			->method('setApp')
			->with(Application::APP_NAME)
			->willReturn($event);

		$event->expects($this->once())
			->method('setType')
			->with('account_deletion')
			->willReturn($event);

		$event->expects($this->once())
			->method('setAuthor')
			->with('someuid')
			->willReturn($event);

		$event->expects($this->once())
			->method('setSubject')
			->with($subject, $options)
			->willReturn($event);

		$event->expects($this->once())
			->method('setAffectedUser')
			->with('someadminuid')
			->willReturn($event);

		return $event;
	}
}
