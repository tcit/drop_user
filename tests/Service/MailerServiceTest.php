<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\DropAccount\Tests\Service;

use ChristophWurst\Nextcloud\Testing\TestCase;
use ChristophWurst\Nextcloud\Testing\TestUser;
use Exception;
use OC_Util;
use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\MissingEmailException;
use OCA\DropAccount\Service\MailerService;
use OCP\Defaults;
use OCP\IAppConfig;
use OCP\IDateTimeFormatter;
use OCP\IL10N;
use OCP\IURLGenerator;
use OCP\IUser;
use OCP\L10N\IFactory;
use OCP\Mail\IMailer;
use PHPUnit\Framework\MockObject\MockObject;

class MailerServiceTest extends TestCase {
	use TestUser;
	/**
	 * @var IAppConfig|MockObject
	 */
	private $appConfig;

	/** @var IUser|(IUser&MockObject)|MockObject */
	private $user;
	private IUser $realUser;
	private MailerService $mailerService;

	/**
	 * @throws Exception
	 */
	public function setUp(): void {
		parent::setUp();
		$this->appConfig = $this->createMock(IAppConfig::class);
		$l10nFactory = $this->createMock(IFactory::class);
		$l10n = $this->createMock(IL10N::class);
		$l10nFactory->method('get')->with(Application::APP_NAME)->willReturn($l10n);
		$mailer = $this->createMock(IMailer::class);
		$urlGenerator = $this->createMock(IURLGenerator::class);
		$defaults = $this->createMock(Defaults::class);
		$dateFormatter = $this->createMock(IDateTimeFormatter::class);
		$this->user = $this->createMock(IUser::class);
		$this->realUser = $this->createTestUser();
		OC_Util::getInstanceId();

		$this->mailerService = new MailerService($this->appConfig, $l10nFactory, $mailer, $defaults, $urlGenerator, $dateFormatter);
	}

	protected function tearDown(): void {
		$this->realUser->delete();

		parent::tearDown();
	}

	public function testSendFinalEmailWithoutEmail(): void {
		$this->expectException(MissingEmailException::class);
		$this->mailerService->sendFinalEmail($this->realUser);
	}

	/**
	 * @throws MissingEmailException
	 * @throws Exception
	 * @dataProvider dataTestSendFinalEmail
	 */
	public function testSendFinalEmail($displayName, bool $delayPurge, int $delayPurgeHours): void {
		$this->expectUser($displayName);
		$matcher = $this->exactly(2);
		$this->appConfig->expects($matcher)->method('getValueString')->willReturnCallback(function ($appId, $key, $value) use ($matcher, $delayPurge, $delayPurgeHours): string {
			$result = [$delayPurge ? 'yes' : 'no', (string)$delayPurgeHours];
			match ($matcher->numberOfInvocations()) {
				1 => $this->assertEquals('delayPurge', $key),
				2 => $this->assertEquals('delayPurgeHours', $key),
			};
			return $result[$matcher->numberOfInvocations() - 1];
		});
		$this->mailerService->sendFinalEmail($this->user);
	}

	public static function dataTestSendFinalEmail(): array {
		return [
			['Someone Named', false, 24],
			[null, false, 24],
			[null, true, 24],
			[null, true, 8]
		];
	}

	/**
	 * @throws MissingEmailException
	 */
	public function testSendReactivationEmail(): void {
		$this->expectUser(null);
		$this->mailerService->sendReactivationEmail($this->user);
	}

	private function expectUser($displayName): void {
		$this->user->expects($this->once())->method('getDisplayName')->with()->willReturn($displayName);
		$this->user->expects($this->once())->method('getEMailAddress')->with()->willReturn('somewhere@else.com');
	}
}
