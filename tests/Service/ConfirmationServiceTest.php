<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\DropAccount\Tests\Service;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\DropAccount\MissingEmailException;
use OCA\DropAccount\Service\ConfirmationService;
use OCP\Defaults;
use OCP\IL10N;
use OCP\IURLGenerator;
use OCP\IUser;
use OCP\Mail\IEMailTemplate;
use OCP\Mail\IMailer;
use OCP\Mail\IMessage;
use OCP\Security\ISecureRandom;
use OCP\Util;
use PHPUnit\Framework\MockObject\MockObject;

class ConfirmationServiceTest extends TestCase {
	private ConfirmationService $confirmationService;
	/**
	 * @var IUser|MockObject
	 */
	private $user;
	/**
	 * @var Defaults|MockObject
	 */
	private $defaults;
	/**
	 * @var IMailer|MockObject
	 */
	private $mailer;

	public function setUp(): void {
		parent::setUp();
		$l10n = $this->createMock(IL10N::class);
		$secureRandom = $this->createMock(ISecureRandom::class);
		$this->mailer = $this->createMock(IMailer::class);
		$this->defaults = $this->createMock(Defaults::class);
		$urlGenerator = $this->createMock(IURLGenerator::class);
		$this->user = $this->createMock(IUser::class);
		$this->user->method('getEMailAddress')->willReturn('some@email.org');
		$secureRandom->method('generate')->with(16, ISecureRandom::CHAR_HUMAN_READABLE)->willReturn('some-token');

		$this->confirmationService = new ConfirmationService($l10n, $secureRandom, $this->mailer, $this->defaults, $urlGenerator);
	}

	public function testSendConfirmationEmailWithNoEmail(): void {
		$user = $this->createMock(IUser::class);

		$user->expects($this->once())->method('getDisplayName')->willReturn(null);
		$user->expects($this->once())->method('getEMailAddress')->willReturn(null);
		$this->expectException(MissingEmailException::class);

		$this->confirmationService->sendConfirmationEmail($user);
	}

	/**
	 * @throws MissingEmailException
	 */
	public function testSendConfirmationEmail(): void {
		$this->user->expects($this->once())->method('getDisplayName')->willReturn('My User');

		$this->defaults->expects($this->exactly(2))->method('getName')->willReturn('fromDefault');

		$template = $this->getTemplateMock();
		$this->mailer->expects($this->once())
			->method('createEMailTemplate')
			->with('drop_account.delete_confirmation')
			->willReturn($template);
		$message = $this->getMessageMock(['some@email.org' => 'My User'], $template);
		$this->mailer->expects($this->once())
			->method('createMessage')
			->with()
			->willReturn($message);
		$this->mailer->expects($this->once())->method('send')->with($message);

		$this->assertEquals('some-token', $this->confirmationService->sendConfirmationEmail($this->user));
	}

	/**
	 * @throws MissingEmailException
	 */
	public function testSendConfirmationEmailWithNoDisplayName(): void {
		$this->user->expects($this->once())->method('getDisplayName')->willReturn(null);
		$this->defaults->expects($this->exactly(2))->method('getName')->willReturn('fromDefault');

		$template = $this->getTemplateMock();
		$this->mailer->expects($this->once())
			->method('createEMailTemplate')
			->with('drop_account.delete_confirmation')
			->willReturn($template);
		$message = $this->getMessageMock(['some@email.org'], $template);
		$this->mailer->expects($this->once())
			->method('createMessage')
			->with()
			->willReturn($message);
		$this->mailer->expects($this->once())->method('send')->with($message);

		$this->assertEquals('some-token', $this->confirmationService->sendConfirmationEmail($this->user));
	}

	private function getTemplateMock(): IEMailTemplate {
		$template = $this->createMock(IEMailTemplate::class);

		$template->expects($this->once())
			->method('addHeader')
			->with()
			->willReturn($template);

		$template->expects($this->once())
			->method('addHeading')
			->with()
			->willReturn($template);

		$template->expects($this->exactly(5))
			->method('addBodyText')
			->with()
			->willReturn($template);

		$template->expects($this->once())
			->method('addBodyButton')
			->with()
			->willReturn($template);

		$template->expects($this->once())
			->method('addFooter')
			->with()
			->willReturn($template);

		return $template;
	}

	/**
	 * @param array $toMail
	 * @param IEMailTemplate $templateMock
	 * @return IMessage
	 */
	private function getMessageMock(array $toMail, IEMailTemplate $templateMock):IMessage {
		$message = $this->createMock(IMessage::class);
		$fromEmail = Util::getDefaultEmailAddress('noreply');

		$message->expects($this->once())
			->method('setFrom')
			->with([$fromEmail => 'fromDefault'])
			->willReturn($message);

		$message->expects($this->once())
			->method('setTo')
			->with($toMail)
			->willReturn($message);

		$message->expects($this->once())
			->method('useTemplate')
			->with($templateMock)
			->willReturn($message);

		return $message;
	}
}
