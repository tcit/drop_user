<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\DropAccount\Tests\Service;

use BadMethodCallException;
use ChristophWurst\Nextcloud\Testing\TestCase;
use ChristophWurst\Nextcloud\Testing\TestUser;
use InvalidArgumentException;
use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\Service\ActivityService;
use OCA\DropAccount\Service\DeleteAccountDataService;
use OCP\Activity\IEvent;
use OCP\IUser;
use OCP\IUserManager;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;

class DeleteAccountDataServiceTest extends TestCase {
	use TestUser;

	/**
	 * @var ActivityService|MockObject
	 */
	private $activityService;
	/**
	 * @var IUserManager|MockObject
	 */
	private $userManager;
	private DeleteAccountDataService $deleteAccountDataService;
	/**
	 * @var IUser|MockObject
	 */
	private $user;
	/**
	 * @var MockObject|LoggerInterface
	 */
	private $logger;

	public function setUp(): void {
		parent::setUp();
		$this->userManager = $this->createMock(IUserManager::class);
		$this->logger = $this->createMock(LoggerInterface::class);
		$this->activityService = $this->createMock(ActivityService::class);

		$this->user = $this->createMock(IUser::class);
		$this->user->method('getUID')->willReturn('someuid');
		$this->user->method('getDisplayName')->willReturn(null);
		$this->user->method('getEMailAddress')->willReturn(null);

		$this->deleteAccountDataService = new DeleteAccountDataService($this->userManager, $this->logger, $this->activityService);
	}

	public function testDelete(): void {
		$this->userManager->expects($this->once())->method('get')->with('someuid')->willReturn($this->user);
		$this->user->expects($this->once())->method('delete')->with()->willReturn(true);
		$this->deleteAccountDataService->delete('someuid');
	}

	public function testDeleteWithUIDFromUnknownUser(): void {
		$this->userManager->expects($this->once())->method('get')->with('someuid')->willReturn(null);
		$this->logger->expects($this->once())->method('error')->with('No user found with UID <someuid>');

		$this->deleteAccountDataService->delete('someuid');
	}

	public function testDeleteWithDeleteError(): void {
		$this->userManager->expects($this->once())->method('get')->with('someuid')->willReturn($this->user);
		$this->user->expects($this->once())->method('delete')->with()->willReturn(false);
		$this->logger->expects($this->once())->method('error')->with('There has been an issue while deleting the user <someuid>.');

		$this->deleteAccountDataService->delete('someuid');
	}

	public function testDeleteWhenUserIsStillEnabled(): void {
		$this->userManager->expects($this->once())->method('get')->with('someuid')->willReturn($this->user);

		$this->user->expects($this->once())->method('isEnabled')->with()->willReturn(true);
		$this->logger->expects($this->once())->method('info')->with('Tried to delete the user <someuid>, but their account is still active. An admin might have saved them.');

		$this->deleteAccountDataService->delete('someuid');
	}

	public function dataProviderTestDeleteWithActivityException(): array {
		return [
			[ new InvalidArgumentException()],
			[ new BadMethodCallException()],
		];
	}

	public function testDeleteWithActivityManagerException(): void {
		$exception = new BadMethodCallException();

		$this->userManager->expects($this->once())->method('get')->with('someuid')->willReturn($this->user);
		$this->user->expects($this->once())->method('delete')->with()->willReturn(true);
		$this->activityService->expects($this->once())->method('createAdminActivities');
		$this->activityService->expects($this->once())->method('sendActivities')->willThrowException($exception);

		$this->logger->expects($this->once())->method('error')->with('There has been an issue sending the delete activity to admins', ['exception' => $exception, 'app' => Application::APP_NAME]);

		$this->deleteAccountDataService->delete('someuid');
	}

	public function testDeleteWithActivities(): void {
		$this->userManager->expects($this->once())->method('get')->with('someuid')->willReturn($this->user);
		$this->activityService->expects($this->once())->method('createAdminActivities');
		$this->activityService->expects($this->once())->method('sendActivities');

		$this->user->expects($this->once())->method('delete')->with()->willReturn(true);

		$this->deleteAccountDataService->delete('someuid');
	}

	private function getActivityEventMock(string $adminUid, string $uid, ?string $name, ?string $email) {
		$event = $this->createMock(IEvent::class);
		$this->activityService->expects($this->once())->method('generateEvent')->with()->willReturn($event);
		$event->expects($this->once())
			->method('setApp')
			->with(Application::APP_NAME)
			->willReturn($event);

		$event->expects($this->once())
			->method('setType')
			->with('account_deletion')
			->willReturn($event);

		$event->expects($this->once())
			->method('setAuthor')
			->with($uid)
			->willReturn($event);

		$event->expects($this->once())
			->method('setSubject')
			->with('account_self_deletion', ['username' => $uid, 'name' => $name, 'email' => $email])
			->willReturn($event);

		$event->expects($this->once())
			->method('setAffectedUser')
			->with($adminUid)
			->willReturn($event);

		return $event;
	}
}
