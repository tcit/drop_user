<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\DropAccount\Tests;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\DropAccount\Capabilities;
use OCA\DropAccount\Service\DeletionCapabilityService;
use OCP\IAppConfig;
use OCP\IConfig;
use OCP\IL10N;
use OCP\IUser;
use OCP\IUserSession;
use PHPUnit\Framework\MockObject\MockObject;

class CapabilitiesTest extends TestCase {

	/** @var IUserSession|(IUserSession&MockObject)|MockObject */
	private $userSession;
	/** @var DeletionCapabilityService|(DeletionCapabilityService&MockObject)|MockObject */
	private $deletionCapabilityService;
	/** @var IL10N|(IL10N&MockObject)|MockObject */
	private $l10n;
	/** @var IAppConfig|(IConfig&MockObject)|MockObject */
	private $appConfig;

	private Capabilities $capabilities;

	protected function setUp(): void {
		parent::setUp();
		$this->userSession = $this->createMock(IUserSession::class);
		$this->deletionCapabilityService = $this->createMock(DeletionCapabilityService::class);
		$this->l10n = $this->createMock(IL10N::class);
		$this->appConfig = $this->createMock(IAppConfig::class);
		$this->capabilities = new Capabilities($this->userSession, $this->deletionCapabilityService, $this->appConfig, $this->l10n);
	}

	public function testCapabilitiesWithNoUser() {
		$this->userSession->expects($this->once())->method('getUser')->willReturn(null);
		$this->l10n->expects($this->once())->method('t')->with('User session not found');

		$this->deletionCapabilityService->expects($this->never())->method('canUserBeDeleted');

		$matcher = $this->exactly(2);
		$this->appConfig->expects($matcher)->method('getValueString')->willReturnCallback(function ($appId, $key, $value) use ($matcher): string {
			$result = ['no', '24'];
			match ($matcher->numberOfInvocations()) {
				1 => $this->assertEquals('delayPurge', $key),
				2 => $this->assertEquals('delayPurgeHours', $key),
			};
			return $result[$matcher->numberOfInvocations() - 1];
		});

		$this->assertEquals(['drop-account' =>
			[
				'enabled' => false,
				'delay' => [
					'enabled' => false,
					'hours' => 24,
				],
				'details' => null,
				'api-version' => 'v1',
			]
		], $this->capabilities->getCapabilities());
	}

	/**
	 * @dataProvider dataForTestCapabilitiesWithUser
	 */
	public function testCapabilitiesWithUser(bool $canBeDeleted, ?string $reason) {
		$user = $this->createMock(IUser::class);
		$this->userSession->expects($this->once())->method('getUser')->willReturn($user);
		$this->deletionCapabilityService->expects($this->once())->method('canUserBeDeleted')->with($user)->willReturn([$canBeDeleted, $reason]);

		$matcher = $this->exactly(2);
		$this->appConfig->expects($matcher)->method('getValueString')->willReturnCallback(function ($appId, $key, $value) use ($matcher): string {
			$result = ['yes', '168'];
			match ($matcher->numberOfInvocations()) {
				1 => $this->assertEquals('delayPurge', $key),
				2 => $this->assertEquals('delayPurgeHours', $key),
			};
			return $result[$matcher->numberOfInvocations() - 1];
		});

		$this->assertEquals(['drop-account' =>
			[
				'enabled' => $canBeDeleted,
				'delay' => [
					'enabled' => true,
					'hours' => 168,
				],
				'details' => $reason,
				'api-version' => 'v1',
			]
		], $this->capabilities->getCapabilities());
	}

	public static function dataForTestCapabilitiesWithUser(): array {
		return [
			[true, null],
			[false, ''],
			[false, ''],
		];
	}
}
