<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\DropAccount\Tests\Settings;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\Settings\Admin;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Services\IInitialState;
use OCP\IAppConfig;
use PHPUnit\Framework\MockObject\MockObject;

class AdminTest extends TestCase {
	private Admin $admin;
	/**
	 * @var IInitialState|MockObject
	 */
	private $initialState;
	/**
	 * @var IAppConfig|MockObject
	 */
	private $appConfig;

	public function setUp(): void {
		parent::setUp();
		$this->appConfig = $this->createMock(IAppConfig::class);
		$this->initialState = $this->createMock(IInitialState::class);

		$this->admin = new Admin($this->appConfig, $this->initialState);
	}

	public function testGetPriority(): void {
		$this->assertEquals(80, $this->admin->getPriority());
	}

	public function testGetSection(): void {
		$this->assertEquals('additional', $this->admin->getSection());
	}

	/**
	 * @param string $requiresConfirmation
	 * @param string $delayPurge
	 * @param string $delayPurgeHours
	 * @param array $result
	 * @dataProvider dataForTestGetForm
	 */
	public function testGetForm(string $requiresConfirmation, string $delayPurge, string $delayPurgeHours, array $result): void {
		$res = new TemplateResponse(Application::APP_NAME, 'admin');

		$matcher = $this->exactly(3);
		$resultLocal = [$requiresConfirmation, $delayPurge, $delayPurgeHours];
		$this->appConfig->expects($matcher)->method('getValueString')->willReturnCallback(function ($appId, $key, $value) use ($matcher, $resultLocal): string {
			match ($matcher->numberOfInvocations()) {
				1 => $this->assertEquals('requireConfirmation', $key),
				2 => $this->assertEquals('delayPurge', $key),
				3 => $this->assertEquals('delayPurgeHours', $key),
			};
			return $resultLocal[$matcher->numberOfInvocations() - 1];
		});

		$matcher = $this->exactly(3);
		$this->appConfig->expects($matcher)->method('getValueString')->willReturnCallback(function ($appId, $key, $value) use ($matcher, $result): mixed {
			match ($matcher->numberOfInvocations()) {
				1 => $this->assertEquals('requireConfirmation', $key),
				2 => $this->assertEquals('delayPurge', $key),
				3 => $this->assertEquals('delayPurgeHours', $key),
			};
			return $result[$key];
		});

		$this->assertEquals($res, $this->admin->getForm());
	}

	public function dataForTestGetForm(): array {
		return [
			[
				'no', 'no', '24', ['requireConfirmation' => false, 'delayPurge' => 'no', 'delayPurgeHours' => '24']
			],
			[
				'yes', 'no', '24', ['requireConfirmation' => true, 'delayPurge' => 'no', 'delayPurgeHours' => '24']
			],
			[
				'yes', 'yes', '24', ['requireConfirmation' => true, 'delayPurge' => 'yes', 'delayPurgeHours' => '24']
			],
			[
				'yes', 'yes', '720', ['requireConfirmation' => true, 'delayPurge' => 'yes', 'delayPurgeHours' => '720']
			],
			[
				'no', 'yes', '168', ['requireConfirmation' => false, 'delayPurge' => 'yes', 'delayPurgeHours' => '168']
			],
		];
	}
}
