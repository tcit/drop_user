<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\DropAccount\Tests\Settings;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\Settings\PersonalSection;
use OCP\IL10N;
use OCP\IURLGenerator;
use PHPUnit\Framework\MockObject\MockObject;

class PersonalSectionTest extends TestCase {
	private PersonalSection $personalSection;
	/**
	 * @var IURLGenerator|MockObject
	 */
	private $urlGenerator;

	public function setUp(): void {
		parent::setUp();
		$this->urlGenerator = $this->createMock(IURLGenerator::class);
		$l10n = $this->createMock(IL10N::class);
		$l10n
			->method('t')
			->willReturnCallback(function ($string, $args) {
				return 'TRANSLATED: ' . vsprintf($string, $args);
			});

		$this->personalSection = new PersonalSection($this->urlGenerator, $l10n);
	}

	public function testGetID(): void {
		$this->assertEquals(Application::APP_NAME, $this->personalSection->getID());
	}

	public function testGetName(): void {
		$this->assertEquals('TRANSLATED: Delete account', $this->personalSection->getName());
	}

	public function testGetPriority(): void {
		$this->assertEquals(90, $this->personalSection->getPriority());
	}

	public function testGetIcon(): void {
		$this->urlGenerator->expects($this->once())->method('imagePath')->with('core', 'actions/delete.svg');
		$this->personalSection->getIcon();
	}
}
