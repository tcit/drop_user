<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\DropAccount\Tests\AppInfo;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\DropAccount\AppInfo\Application;

class ApplicationTest extends TestCase {
	public function testConstructor(): void {
		$this->assertInstanceOf(Application::class, new Application());
	}
}
