<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\DropAccount\Tests\Listeners;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\BackgroundJob\DeleteAccountData;
use OCA\DropAccount\Listeners\UserReEnabledListener;
use OCA\DropAccount\Service\ActivityService;
use OCA\DropAccount\Service\MailerService;
use OCP\BackgroundJob\IJobList;
use OCP\EventDispatcher\Event;
use OCP\IConfig;
use OCP\IUser;
use OCP\User\Events\UserChangedEvent;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;

class UserReEnabledListenerTest extends TestCase {
	/**
	 * @var IConfig|MockObject
	 */
	private $config;
	/**
	 * @var IJobList|MockObject
	 */
	private $jobList;

	/** @var MailerService|(MailerService&MockObject)|MockObject */
	private $mailer;

	/** @var ActivityService|(ActivityService&MockObject)|MockObject */
	private $activityService;

	private UserReEnabledListener $listener;

	public const UID = 'someUID';
	/**
	 * @var UserChangedEvent|MockObject
	 */
	private $event;

	public function setUp(): void {
		parent::setUp();
		$this->jobList = $this->createMock(IJobList::class);
		$this->config = $this->createMock(IConfig::class);
		$this->event = $this->createMock(UserChangedEvent::class);
		$logger = $this->createMock(LoggerInterface::class);
		$this->mailer = $this->createMock(MailerService::class);
		$this->activityService = $this->createMock(ActivityService::class);

		$this->listener = new UserReEnabledListener($this->jobList, $this->config, $this->mailer, $this->activityService, $logger);
	}

	public function testHandleUnrelated(): void {
		$event = new Event();
		$this->config->expects($this->never())->method('deleteUserValue');
		$this->listener->handle($event);
	}

	public function testHandleNotEnabledChanged(): void {
		$this->configureEventMock('something', 'old', 'new');
		$this->config->expects($this->never())->method('deleteUserValue');
		$this->listener->handle($this->event);
	}

	public function testHandleDisabled(): void {
		$this->configureEventMock('enabled', true, false);
		$this->config->expects($this->never())->method('deleteUserValue');
		$this->listener->handle($this->event);
	}

	/**
	 * @dataProvider dataForTestHandleEnabled
	 */
	public function testHandleEnabled(bool $markedForPurge): void {
		$user = $this->configureEventMock();
		$this->jobList->expects($this->once())->method('has')->with(DeleteAccountData::class, ['uid' => self::UID])->willReturn(false);
		$this->jobList->expects($this->never())->method('remove')->with(DeleteAccountData::class, ['uid' => self::UID]);
		$this->config->expects($this->once())->method('getUserValue')->with(self::UID, Application::APP_NAME, 'markedForPurge')->willReturn($markedForPurge ? 'yes' : 'no', null);
		if ($markedForPurge) {
			$this->mailer->expects($this->once())->method('sendReactivationEmail')->with($user);
			$this->activityService->expects($this->once())
				->method('createAdminActivities')->with($user, ['subject' => 'account_re_enabled']);
			$this->activityService->expects($this->once())
				->method('sendActivities');
		}

		$matcher = $this->exactly(2);
		$this->config->expects($matcher)->method('deleteUserValue')->willReturnCallback(function ($userId, $appId, $key) use ($matcher): void {
			match ($matcher->numberOfInvocations()) {
				1 => $this->assertEquals('markedForPurge', $key),
				2 => $this->assertEquals('purgeDate', $key),
			};
		});

		$this->listener->handle($this->event);
	}

	public function dataForTestHandleEnabled(): array {
		return [
			[true],
			[false]
		];
	}

	public function testHandleEnabledWithClearJobs(): void {
		$this->configureEventMock();
		$this->jobList->expects($this->once())->method('has')->with(DeleteAccountData::class, ['uid' => self::UID])->willReturn(true);
		$this->jobList->expects($this->once())->method('remove')->with(DeleteAccountData::class, ['uid' => self::UID]);

		$matcher = $this->exactly(2);
		$this->config->expects($matcher)->method('deleteUserValue')->willReturnCallback(function ($userId, $appId, $key) use ($matcher): void {
			match ($matcher->numberOfInvocations()) {
				1 => $this->assertEquals('markedForPurge', $key),
				2 => $this->assertEquals('purgeDate', $key),
			};
		});
		$this->listener->handle($this->event);
	}

	private function configureEventMock(string $feature = 'enabled', $oldValue = false, $newValue = true): IUser {
		$user = $this->createMock(IUser::class);
		$user->expects($this->once())->method('getUID')->willReturn(self::UID);
		$this->event->expects($this->once())->method('getUser')->willReturn($user);
		$this->event->expects($this->once())->method('getFeature')->willReturn($feature);
		$this->event->expects($this->once())->method('getOldValue')->willReturn($oldValue);
		$this->event->expects($this->once())->method('getValue')->willReturn($newValue);
		return $user;
	}
}
