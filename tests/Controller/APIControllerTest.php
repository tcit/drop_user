<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\DropAccount\Tests\Controller;

use ChristophWurst\Nextcloud\Testing\TestCase;
use Exception;
use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\BackgroundJob\DeleteAccountData;
use OCA\DropAccount\Controller\APIController;
use OCA\DropAccount\MissingEmailException;
use OCA\DropAccount\Service\ConfirmationService;
use OCA\DropAccount\Service\DeletionCapabilityService;
use OCA\DropAccount\Service\MailerService;
use OCP\AppFramework\Http;
use OCP\BackgroundJob\IJobList;
use OCP\IAppConfig;
use OCP\IConfig;
use OCP\IL10N;
use OCP\IRequest;
use OCP\IUser;
use OCP\IUserSession;
use OCP\PreConditionNotMetException;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;

class APIControllerTest extends TestCase {
	/** @var IUserSession|MockObject */
	private $userSession;

	/** @var IJobList|MockObject */
	private $jobList;

	/** @var IConfig|MockObject */
	private $config;

	/** @var IAppConfig|MockObject */
	private $appConfig;

	/** @var ConfirmationService|MockObject */
	private $confirmationService;

	/** @var DeletionCapabilityService|(DeletionCapabilityService&MockObject)|MockObject */
	private $deletionCapabilityService;

	/**
	 * @var IUser|MockObject
	 */
	private $user;
	private APIController $controller;

	public const USER_EMAIL = 'user@email.com';

	public function setUp(): void {
		parent::setUp();
		$request = $this->createMock(IRequest::class);
		$l10n = $this->createMock(IL10N::class);
		$this->userSession = $this->createMock(IUserSession::class);
		$logger = $this->createMock(LoggerInterface::class);
		$this->jobList = $this->createMock(IJobList::class);
		$this->config = $this->createMock(IConfig::class);
		$this->appConfig = $this->createMock(IAppConfig::class);
		$this->confirmationService = $this->createMock(ConfirmationService::class);
		$mailerService = $this->createMock(MailerService::class);
		$this->deletionCapabilityService = $this->createMock(DeletionCapabilityService::class);
		$this->user = $this->createMock(IUser::class);
		$this->user->method('getDisplayName')->willReturn('User Displayname 123');
		$this->user->method('getEMailAddress')->willReturn(self::USER_EMAIL);

		$l10n
			->method('t')
			->willReturnCallback(function ($string, $args) {
				return 'TRANSLATED: ' . vsprintf($string, $args);
			});

		$this->controller = new APIController(
			Application::APP_NAME,
			$request,
			$this->userSession,
			$logger,
			$this->config,
			$this->appConfig,
			$l10n,
			$this->jobList,
			$this->confirmationService,
			$mailerService,
			$this->deletionCapabilityService
		);
	}

	/**
	 * @throws PreConditionNotMetException
	 */
	public function testUserSessionExpired(): void {
		$this->userSession->expects($this->once())
			->method('getUser')
			->with()
			->willReturn(null);

		$response = $this->controller->delete();
		$this->assertEquals([
			'message' => 'TRANSLATED: User-Session unexpectedly expired'
		], $response->getData());
		$this->assertEquals(401, $response->getStatus());
	}


	public function testDeletionCapacity(): void {
		$this->userSession->expects($this->once())
			->method('getUser')
			->with()
			->willReturn($this->user);

		$this->deletionCapabilityService->expects($this->once())->method('canUserBeDeleted')->with($this->user)->willReturn([false, 'Some reason']);

		$response = $this->controller->delete();

		$this->assertEquals([
			'message' => 'Some reason'
		], $response->getData());
		$this->assertEquals(Http::STATUS_FORBIDDEN, $response->getStatus());
	}

	/**
	 * @throws PreConditionNotMetException
	 */
	public function testSendConfirmationEmail(): void {
		$this->userSession->expects($this->once())
			->method('getUser')
			->with()
			->willReturn($this->user);

		$this->deletionCapabilityService->expects($this->once())->method('canUserBeDeleted')->with($this->user)->willReturn([true, null]);

		$this->appConfig->expects($this->once())
			->method('getValueString')
			->with(Application::APP_NAME, 'requireConfirmation', 'no')
			->willReturn('yes');

		$this->confirmationService->expects($this->once())
			->method('sendConfirmationEmail')
			->with($this->user)
			->willReturn(self::USER_EMAIL);

		$response = $this->controller->delete();

		$this->assertEquals([
			'message' => 'TRANSLATED: Successfully sent email'
		], $response->getData());
		$this->assertEquals(Http::STATUS_CREATED, $response->getStatus());
	}

	/**
	 * @throws PreConditionNotMetException
	 */
	public function testSendConfirmationEmailWithoutUserEmail(): void {
		$user = $this->createMock(IUser::class);
		$user->method('getEMailAddress')->willReturn(null);

		$this->userSession->expects($this->once())
			->method('getUser')
			->with()
			->willReturn($user);

		$this->deletionCapabilityService->expects($this->once())->method('canUserBeDeleted')->with($this->user)->willReturn([true, null]);

		$this->appConfig->expects($this->once())
			->method('getValueString')
			->with(Application::APP_NAME, 'requireConfirmation', 'no')
			->willReturn('yes');

		$this->confirmationService->expects($this->once())
			->method('sendConfirmationEmail')
			->with($user)
			->willThrowException(new MissingEmailException());

		$response = $this->controller->delete();

		$this->assertEquals([
			'message' => 'TRANSLATED: You have no email set up into your account'
		], $response->getData());
		$this->assertEquals(Http::STATUS_BAD_REQUEST, $response->getStatus());
	}

	/**
	 * @throws PreConditionNotMetException
	 */
	public function testSendConfirmationEmailWithoutMailerFailure(): void {
		$this->userSession->expects($this->once())
			->method('getUser')
			->with()
			->willReturn($this->user);

		$this->deletionCapabilityService->expects($this->once())->method('canUserBeDeleted')->with($this->user)->willReturn([true, null]);

		$this->appConfig->expects($this->once())
			->method('getValueString')
			->with(Application::APP_NAME, 'requireConfirmation', 'no')
			->willReturn('yes');

		$this->confirmationService->expects($this->once())
			->method('sendConfirmationEmail')
			->with($this->user)
			->willThrowException(new Exception('Something bad happened'));

		$response = $this->controller->delete();

		$this->assertEquals([
			'message' => 'TRANSLATED: Unexpected error sending email. Please contact your administrator.'
		], $response->getData());
		$this->assertEquals(Http::STATUS_INTERNAL_SERVER_ERROR, $response->getStatus());
	}

	/**
	 * @dataProvider dataTestDeleteAccount
	 * @param string $delayPurge
	 * @throws PreConditionNotMetException
	 */
	public function testDeleteAccount(string $delayPurge): void {
		$this->user->expects($this->exactly($delayPurge === 'yes' ? 3 : 2))
			->method('getUID')
			->with()
			->willReturn('userid');

		$this->userSession->expects($this->once())
			->method('getUser')
			->with()
			->willReturn($this->user);

		$this->deletionCapabilityService->expects($this->once())->method('canUserBeDeleted')->with($this->user)->willReturn([true, null]);

		$matcher = $this->exactly(2);
		$this->appConfig->expects($matcher)->method('getValueString')->willReturnCallback(function ($appId, $key, $value) use ($matcher, $delayPurge): string {
			$result = ['no', $delayPurge];
			match ($matcher->numberOfInvocations()) {
				1 => $this->assertEquals('requireConfirmation', $key),
				2 => $this->assertEquals('delayPurge', $key),
			};
			return $result[$matcher->numberOfInvocations() - 1];
		});

		$this->user->expects($this->once())
			->method('setEnabled')
			->with(false);

		if ($delayPurge === 'yes') {
			$this->config
				->expects($this->exactly(2))
				->method('setUserValue');
		} else {
			$this->jobList->expects($this->once())
				->method('add')
				->with(DeleteAccountData::class, ['uid' => 'userid']);
		}

		$response = $this->controller->delete();

		$this->assertEquals([], $response->getData());
		$this->assertEquals(Http::STATUS_ACCEPTED, $response->getStatus());
	}

	public function dataTestDeleteAccount(): array {
		return [
			[
				'no'
			],
			[
				'yes'
			]
		];
	}
}
