<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\DropAccount\Tests\BackgroundJob;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\DropAccount\BackgroundJob\DeleteAccountData;
use OCA\DropAccount\Service\DeleteAccountDataService;
use OCP\AppFramework\Utility\ITimeFactory;
use PHPUnit\Framework\MockObject\MockObject;

class DeleteAccountDataTest extends TestCase {
	private DeleteAccountData $deleteAccountDataJob;
	/**
	 * @var DeleteAccountDataService|MockObject
	 */
	private $service;

	public function setUp(): void {
		parent::setUp();
		$timeFactory = $this->createMock(ITimeFactory::class);
		$this->service = $this->createMock(DeleteAccountDataService::class);
		$this->deleteAccountDataJob = new DeleteAccountData($timeFactory, $this->service);
	}

	public function test(): void {
		$this->service->expects($this->once())->method('delete')->with('toto');
		$this->deleteAccountDataJob->run(['uid' => 'toto']);
	}

	public function testWithEmptyArray(): void {
		$this->expectExceptionMessage('DeleteAccountData job requires an uid argument');
		$this->deleteAccountDataJob->run([]);
	}
}
