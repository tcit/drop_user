/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

module.exports = {
	extends: ['@nextcloud', 'prettier'],
	overrides: [
		{
			files: ['**/*.vue'],
			rules: {
				'vue/first-attribute-linebreak': 'off',
			},
		},
	],
}
