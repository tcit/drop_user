<?php

/**
 * SPDX-FileCopyrightText: 2017 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

namespace OCA\DropAccount\Activity;

use OCP\Activity\ISetting;
use OCP\IL10N;

class Setting implements ISetting {

	public function __construct(
		private IL10N $l,
	) {
	}

	/**
	 * @return string Lowercase a-z and underscore only identifier
	 * @since 11.0.0
	 * @psalm-return 'account_deletion'
	 */
	public function getIdentifier(): string {
		return 'account_deletion';
	}

	/**
	 * @return string A translated string
	 * @since 11.0.0
	 */
	public function getName(): string {
		return $this->l->t('A user <strong>deleted</strong> its account');
	}

	/**
	 * @return int whether the filter should be rather on the top or bottom of
	 *             the admin section. The filters are arranged in ascending order of the
	 *             priority values. It is required to return a value between 0 and 100.
	 * @since 11.0.0
	 * @psalm-return 80
	 */
	public function getPriority(): int {
		return 80;
	}

	/**
	 * @return true True when the option can be changed for the stream
	 * @since 11.0.0
	 */
	public function canChangeStream(): bool {
		return true;
	}

	/**
	 * @return true True when the option can be changed for the stream
	 * @since 11.0.0
	 */
	public function isDefaultEnabledStream(): bool {
		return true;
	}

	/**
	 * @return true True when the option can be changed for the mail
	 * @since 11.0.0
	 */
	public function canChangeMail(): bool {
		return true;
	}

	/**
	 * @return false True when the option can be changed for the stream
	 * @since 11.0.0
	 */
	public function isDefaultEnabledMail(): bool {
		return false;
	}
}
