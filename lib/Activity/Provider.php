<?php

/**
 * SPDX-FileCopyrightText: 2017 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

namespace OCA\DropAccount\Activity;

use InvalidArgumentException;
use OCA\DropAccount\AppInfo\Application;
use OCP\Activity\IEvent;
use OCP\Activity\IEventMerger;
use OCP\Activity\IProvider;
use OCP\IL10N;
use OCP\IURLGenerator;
use OCP\L10N\IFactory;

class Provider implements IProvider {
	private IL10N $l;

	public function __construct(
		private IFactory $languageFactory,
		private IURLGenerator $url,
		private IEventMerger $eventMerger,
	) {
		$this->l = $this->languageFactory->get(Application::APP_NAME);
	}

	/**
	 * @param string $language The language which should be used for translating, e.g. "en"
	 * @param IEvent $event The current event which should be parsed
	 * @param IEvent|null $previousEvent A potential previous event which you can combine with the current one.
	 *                                   To do so, simply use setChildEvent($previousEvent) after setting the
	 *                                   combined subject on the current event.
	 * @return IEvent
	 * @throws InvalidArgumentException Should be thrown if your provider does not know this event
	 */
	public function parse($language, IEvent $event, ?IEvent $previousEvent = null): IEvent {
		if ($event->getApp() !== Application::APP_NAME || $event->getType() !== 'account_deletion') {
			throw new InvalidArgumentException();
		}

		$this->l = $this->languageFactory->get(Application::APP_NAME, $language);
		$event->setIcon($this->url->imagePath('core', 'actions/delete.svg'));

		return $this->parseShortVersion($event, $previousEvent);
	}

	/**
	 * @param IEvent $event
	 * @param IEvent|null $previousEvent
	 * @return IEvent
	 */
	private function parseShortVersion(IEvent $event, ?IEvent $previousEvent = null): IEvent {
		/**
		 * @var array{username?: string, name?: string, email?: string, expiration?: int} $parameters
		 */
		$parameters = $event->getSubjectParameters();
		if (isset($parameters['email'], $parameters['username'], $parameters['name']) && $parameters['username'] !== $parameters['name']) {
			switch ($event->getSubject()) {
				case 'account_self_deletion':
					$event->setParsedSubject($this->l->t('%1$s (%2$s - %3$s) deleted their account', [$parameters['name'], $parameters['username'], $parameters['email']]));
					break;
				case 'account_scheduled_self_deletion':
					$event->setParsedSubject($this->l->t('%1$s (%2$s - %3$s) requested to delete their account. It has been disabled and will automatically be removed on %4$s', [$parameters['name'], $parameters['username'], $parameters['email'], $this->generateExpirationDate($parameters)]));
					break;
				case 'account_re_enabled':
					$event->setParsedSubject($this->l->t('Account %1$s (%2$s - %3$s) has been re-enabled by an admin', [$parameters['name'], $parameters['username'], $parameters['email']]));
					break;
			}
		} elseif (isset($parameters['email'], $parameters['name'])) {
			switch ($event->getSubject()) {
				case 'account_self_deletion':
					$event->setParsedSubject($this->l->t('%1$s (%2$s) deleted their account', [$parameters['name'], $parameters['email']]));
					break;
				case 'account_scheduled_self_deletion':
					$event->setParsedSubject($this->l->t('%1$s (%2$s) requested to delete their account. It has been disabled and will automatically be removed on %3$s', [$parameters['name'], $parameters['email'], $this->generateExpirationDate($parameters)]));
					break;
				case 'account_re_enabled':
					$event->setParsedSubject($this->l->t('Account %1$s (%2$s) has been re-enabled by an admin', [$parameters['name'], $parameters['email']]));
					break;
			}
		} elseif (isset($parameters['username'], $parameters['name']) && $parameters['username'] !== $parameters['name']) {
			switch ($event->getSubject()) {
				case 'account_self_deletion':
					$event->setParsedSubject($this->l->t('%1$s (%2$s) deleted their account', [$parameters['name'], $parameters['username']]));
					break;
				case 'account_scheduled_self_deletion':
					$event->setParsedSubject($this->l->t('%1$s (%2$s) requested to delete their account. It has been disabled and will automatically be removed on %3$s', [$parameters['name'], $parameters['username'], $this->generateExpirationDate($parameters)]));
					break;
				case 'account_re_enabled':
					$event->setParsedSubject($this->l->t('Account %1$s (%2$s) has been re-enabled by an admin', [$parameters['name'], $parameters['username']]));
					break;
			}
		} elseif (isset($parameters['username'])) {
			switch ($event->getSubject()) {
				case 'account_self_deletion':
					$event->setParsedSubject($this->l->t('%1$s deleted their account', [$parameters['name'] ?? $parameters['username']]));
					break;
				case 'account_scheduled_self_deletion':
					$event->setParsedSubject($this->l->t('%1$s requested to delete their account. It has been disabled and will automatically be removed on %2$s', [$parameters['name'] ?? $parameters['username'], $this->generateExpirationDate($parameters)]));
					break;
				case 'account_re_enabled':
					$event->setParsedSubject($this->l->t('Account %1$s has been re-enabled by an admin', [$parameters['name'] ?? $parameters['username']]));
					break;
			}
		} else {
			throw new InvalidArgumentException('Missing required username in parameters');
		}
		return $this->eventMerger->mergeEvents('username', $event, $previousEvent);
	}

	/**
	 * @param array{email?: string, expiration?: int, name?: string, username?: string} $parameters
	 * @return string
	 */
	private function generateExpirationDate(array $parameters): string {
		if (isset($parameters['expiration'])) {
			$localizedDate = $this->l->l('datetime', $parameters['expiration'], ['width' => 'medium|short']);
			if (is_string($localizedDate)) {
				return $localizedDate;
			}
		}
		return '';
	}
}
