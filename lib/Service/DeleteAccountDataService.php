<?php

/**
 * SPDX-FileCopyrightText: 2017 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

namespace OCA\DropAccount\Service;

use BadMethodCallException;
use InvalidArgumentException;
use OCA\DropAccount\AppInfo\Application;
use OCP\IUserManager;
use Psr\Log\LoggerInterface;

class DeleteAccountDataService {

	public function __construct(
		private IUserManager $userManager,
		private LoggerInterface $logger,
		private ActivityService $activityService,
	) {
	}

	public function delete(string $uid): void {
		try {
			$user = $this->userManager->get($uid);

			if (!$user) {
				$this->logger->error("No user found with UID <$uid>");
				return;
			}

			if ($user->isEnabled()) {
				$this->logger->info("Tried to delete the user <$uid>, but their account is still active. An admin might have saved them.");
				return;
			}

			$this->activityService->createAdminActivities($user);

			if (!$user->delete()) {
				$this->logger->error('There has been an issue while deleting the user <' . $uid . '>.');
				return;
			}

			$this->activityService->sendActivities();
		} catch (InvalidArgumentException|BadMethodCallException $e) {
			$this->logger->error('There has been an issue sending the delete activity to admins', ['app' => Application::APP_NAME, 'exception' => $e]);
		}
	}
}
