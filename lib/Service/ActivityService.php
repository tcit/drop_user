<?php

/**
 * SPDX-FileCopyrightText: 2023 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

namespace OCA\DropAccount\Service;

use OCA\DropAccount\AppInfo\Application;
use OCP\Activity\IEvent;
use OCP\Activity\IManager;
use OCP\IGroupManager;
use OCP\IUser;

class ActivityService {


	/**
	 * @var IEvent[]
	 */
	private array $events = [];

	public function __construct(
		private IManager $activityManager,
		private IGroupManager $groupManager,
	) {
	}

	/**
	 * @param array{subject?: string, subject_params?: array{username?: string, name?: string, email?: string, expiration?: int}} $options
	 */
	public function createAdminActivities(IUser $user, array $options = []): void {
		$adminGroup = $this->groupManager->get('admin');
		$admins = [];
		if ($adminGroup) {
			$admins = $adminGroup->getUsers();
		}
		$this->events = [];
		foreach ($admins as $admin) {
			$this->events[] = $this->createActivity($user, $admin, $options);
		}
	}

	public function sendActivities(): void {
		foreach ($this->events as $event) {
			$this->activityManager->publish($event);
		}
	}

	/**
	 * @param array{subject?: string, subject_params?: array{username?: string, name?: string, email?: string, expiration?: int}} $options
	 */
	private function createActivity(IUser $user, IUser $admin, array $options = []): IEvent {
		/**
		 * To be sure that it's accessible once deleted?
		 */
		$username = $user->getUID();
		$name = $user->getDisplayName();
		$email = $user->getEMailAddress();

		$subject = $options['subject'] ?? 'account_self_deletion';

		$event = $this->activityManager->generateEvent();
		$event
			->setApp(Application::APP_NAME)
			->setType('account_deletion')
			->setAuthor($username)
			->setSubject($subject, array_merge($options['subject_params'] ?? [], ['username' => $username, 'name' => $name, 'email' => $email]))
			->setAffectedUser($admin->getUID())
		;

		return $event;
	}
}
