<?php

/**
 * SPDX-FileCopyrightText: 2017 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

namespace OCA\DropAccount\BackgroundJob;

use DateInterval;
use DateTime;
use OCA\DropAccount\AppInfo\Application;
use OCA\DropAccount\Service\DeleteAccountDataService;
use OCP\AppFramework\Utility\ITimeFactory;
use OCP\BackgroundJob\TimedJob;
use OCP\IAppConfig;
use OCP\IConfig;
use Psr\Log\LoggerInterface;

class TimedDeleteAccountData extends TimedJob {
	public function __construct(
		protected ITimeFactory $time,
		private IConfig $config,
		private IAppConfig $appConfig,
		private DeleteAccountDataService $service,
		private LoggerInterface $logger,
	) {
		parent::__construct($time);

		$this->setInterval(3600);
	}

	/**
	 * @param array $argument
	 * @return void
	 */
	public function run($argument) {
		/** @var string[] $userIds */
		$userIds = $this->config->getUsersForUserValue(Application::APP_NAME, 'markedForPurge', 'yes');
		$purgePeriod = $this->appConfig->getValueString(Application::APP_NAME, 'delayPurgeHours', '24');

		foreach ($userIds as $uid) {
			$this->logger->debug(sprintf('Checking if user <%s> should be purged...', $uid));
			$after = (int)$this->config->getUserValue($uid, Application::APP_NAME, 'purgeDate', 0);
			$after = (new DateTime())->setTimestamp($after);
			$duration = 'PT' . $purgePeriod . 'H';
			$after->add(new DateInterval($duration));
			if ($after < new DateTime()) {
				$this->logger->info(sprintf('Purge date of user <%s> has been reached (%s after %s), going to purge', $uid, $duration, $after->format('Y-m-d H:i:s')));
				$this->service->delete($uid);
			}
		}
	}
}
