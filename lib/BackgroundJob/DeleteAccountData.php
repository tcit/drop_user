<?php

/**
 * SPDX-FileCopyrightText: 2017 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

namespace OCA\DropAccount\BackgroundJob;

use InvalidArgumentException;
use OCA\DropAccount\Service\DeleteAccountDataService;
use OCP\AppFramework\Utility\ITimeFactory;
use OCP\BackgroundJob\QueuedJob;

class DeleteAccountData extends QueuedJob {
	public function __construct(
		protected ITimeFactory $time,
		private DeleteAccountDataService $service,
	) {
		parent::__construct($time);
	}

	/**
	 * @param string[] $argument
	 * @return void
	 */
	public function run($argument) {
		if (!isset($argument['uid'])) {
			throw new InvalidArgumentException('DeleteAccountData job requires an uid argument');
		}
		$uid = $argument['uid'];

		$this->service->delete($uid);
	}
}
