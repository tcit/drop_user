<?php

/**
 * SPDX-FileCopyrightText: 2017 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

namespace OCA\DropAccount\Settings;

use OCA\DropAccount\AppInfo\Application;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Services\IInitialState;
use OCP\IAppConfig;
use OCP\Settings\ISettings;
use OCP\Util;

class Admin implements ISettings {

	public function __construct(
		private IAppConfig $appConfig,
		private IInitialState $initialState,
	) {
	}

	/**
	 * @return TemplateResponse
	 */
	public function getForm(): TemplateResponse {
		$requiresConfirmation = $this->appConfig->getValueString(Application::APP_NAME, 'requireConfirmation', 'no') === 'yes';
		$delayPurge = $this->appConfig->getValueString(Application::APP_NAME, 'delayPurge', 'no');
		$delayPurgeHours = $this->appConfig->getValueString(Application::APP_NAME, 'delayPurgeHours', '24');

		$this->initialState->provideInitialState('requireConfirmation', $requiresConfirmation);
		$this->initialState->provideInitialState('delayPurge', $delayPurge);
		$this->initialState->provideInitialState('delayPurgeHours', $delayPurgeHours);
		Util::addStyle(Application::APP_NAME, 'drop_account-admin-settings');
		Util::addScript(Application::APP_NAME, 'drop_account-admin-settings');

		return new TemplateResponse(Application::APP_NAME, 'admin');
	}

	/**
	 * @return string
	 * @psalm-return 'additional'
	 */
	public function getSection(): string {
		return 'additional';
	}

	/**
	 * @return int
	 * @psalm-return 80
	 */
	public function getPriority(): int {
		return 80;
	}
}
