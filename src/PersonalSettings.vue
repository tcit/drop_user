<!--
SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>

SPDX-License-Identifier: AGPL-3.0-only
-->

<template>
	<NcSettingsSection
		:name="t('drop_account', 'Delete my account')"
		:description="
			t(
				'drop_account',
				'Deleting your account will delete all your files and data from the apps you use, such as calendar and contacts.',
			)
		">
		<NcEmptyContent
			v-if="deleted"
			:name="t('drop_account', 'Account marked for deletion')">
			<template #icon>
				<DeleteIcon :size="20" />
			</template>
			<template #action>
				<p>
					{{
						t(
							'drop_account',
							'Your account has been disabled and the data will be removed shortly.',
						)
					}}
				</p>
				<p>
					{{
						t(
							'drop_account',
							'You are going to be redirected to the login page in a few seconds…',
						)
					}}
				</p>
			</template>
		</NcEmptyContent>
		<NcEmptyContent
			v-else-if="confirmationSent"
			:name="t('drop_account', 'Email confirmation required')">
			<template #icon>
				<EMailIcon :size="20" />
			</template>
			<template #action>
				<p>
					{{
						t(
							'drop_account',
							"Please click the link into the email we've just sent you to finish deleting your account.",
						)
					}}
				</p>
			</template>
		</NcEmptyContent>
		<div v-else id="delete-account-settings">
			<NcNoteCard v-if="willDelayPurge" type="info">
				{{
					n(
						'drop_account',
						'This action will be reversible by an administrator for {nbDays} day after you request deletion.',
						'This action will be reversible by an administrator for {nbDays} days after you request deletion.',
						nbDaysForPurge,
						{ nbDays: nbDaysForPurge },
					)
				}}
			</NcNoteCard>
			<NcNoteCard v-else type="error">
				<b>{{ t('drop_account', 'This action is irreversible!') }}</b>
			</NcNoteCard>
			<p v-if="!requireConfirmation" class="settings-hint">
				{{
					t(
						'drop_account',
						'After confirming the deletion of your account, you will be redirected to the login page.',
					)
				}}
			</p>
			<NcNoteCard
				v-if="!canDeleteAccount && deletionImpossibleReason"
				type="warning">
				{{ deletionImpossibleReason }}
			</NcNoteCard>
			<NcNoteCard v-else-if="!canDeleteAccount" type="warning">
				{{
					t(
						'drop_account',
						"You can't delete your account for an unknown reason.",
					)
				}}
			</NcNoteCard>
			<NcNoteCard v-if="!emailIfConfirmation" type="warning">
				{{
					t(
						'drop_account',
						'An email confirmation is required by your admin to delete your account. Please fill your email in your personal settings first.',
					)
				}}
			</NcNoteCard>
			<div v-if="canDeleteAccount && emailIfConfirmation">
				<h3>
					{{
						t(
							'drop_account',
							'Do you really wish to delete your account?',
						)
					}}
				</h3>
				<p v-if="requireConfirmation">
					{{
						t(
							'drop_account',
							'We will send you an email to confirm this action.',
						)
					}}
				</p>
				<NcCheckboxRadioSwitch :checked.sync="checked">
					{{
						t(
							'drop_account',
							'Check this to confirm the deletion request',
						)
					}}
				</NcCheckboxRadioSwitch>
				<br />
				<p class="delete-button-wrapper">
					<NcButton
						:disabled="!checked"
						type="primary"
						@click="deleteAccount">
						<template #icon>
							<span class="icon icon-delete" />
						</template>
						{{ t('drop_account', 'Delete my account') }}
					</NcButton>
				</p>
			</div>
		</div>
	</NcSettingsSection>
</template>
<script>
import { loadState } from '@nextcloud/initial-state'
import Axios from '@nextcloud/axios'
import { generateOcsUrl } from '@nextcloud/router'
import { showError } from '@nextcloud/dialogs'
import { confirmPassword } from '@nextcloud/password-confirmation'
import NcSettingsSection from '@nextcloud/vue/dist/Components/NcSettingsSection.js'
import NcEmptyContent from '@nextcloud/vue/dist/Components/NcEmptyContent.js'
import NcButton from '@nextcloud/vue/dist/Components/NcButton.js'
import NcCheckboxRadioSwitch from '@nextcloud/vue/dist/Components/NcCheckboxRadioSwitch.js'
import NcNoteCard from '@nextcloud/vue/dist/Components/NcNoteCard.js'
import '@nextcloud/password-confirmation/dist/style.css'
import '@nextcloud/dialogs/style.css'
import DeleteIcon from 'vue-material-design-icons/Delete.vue'
import EMailIcon from 'vue-material-design-icons/Email.vue'

export default {
	name: 'PersonalSettings',
	components: {
		NcSettingsSection,
		NcEmptyContent,
		NcButton,
		NcCheckboxRadioSwitch,
		NcNoteCard,
		DeleteIcon,
		EMailIcon,
	},
	data() {
		return {
			canDeleteAccount: true,
			deletionImpossibleReason: false,
			requireConfirmation: false,
			hasEmailForConfirmation: false,
			appName: 'drop_account',
			checked: false,
			deleted: false,
			confirmationSent: false,
			willDelayPurge: false,
			delayPurgeHours: 24,
		}
	},
	computed: {
		emailIfConfirmation() {
			if (this.requireConfirmation) {
				return this.hasEmailForConfirmation
			}
			return true
		},
		nbDaysForPurge() {
			if (this.willDelayPurge && this.delayPurgeHours) {
				return ~~(this.delayPurgeHours / 24)
			}
			return 0
		},
	},
	created() {
		try {
			this.canDeleteAccount = loadState(this.appName, 'can_delete_account')
			this.deletionImpossibleReason = loadState(
				this.appName,
				'deletion_impossible_reason',
			)
			this.requireConfirmation = loadState(
				this.appName,
				'require_confirmation',
			)
			this.hasEmailForConfirmation = loadState(
				this.appName,
				'has_email_for_confirmation',
			)
			this.willDelayPurge = loadState(this.appName, 'will_delay_purge')
			this.delayPurgeHours = loadState(this.appName, 'delay_purge_hours')
		} catch (e) {
			console.error('Error fetching initial state', e)
		}
	},
	methods: {
		async deleteAccount() {
			try {
				await confirmPassword()
				const url = generateOcsUrl(`apps/${this.appName}/api/v1/account`)
				const { status } = await Axios.delete(url, {})
				if (status === 202) {
					this.deleted = true
					setTimeout(() => OC.reload(), 10000)
				} else if (status === 201) {
					this.confirmationSent = true
				}
			} catch (e) {
				console.error(e)
				showError(t('drop_account', 'Error while deleting the account'))
			}
		},
	},
}
</script>
<style scoped>
.delete-button-wrapper {
	margin-top: 1rem;
}
</style>
