/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import Vue from 'vue'
import './common.js'
import AdminSettings from './AdminSettings.vue'

export default new Vue({
	el: '#drop-account-settings',
	render: (h) => h(AdminSettings),
})
