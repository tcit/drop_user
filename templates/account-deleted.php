<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

use OCP\Defaults;
use OCP\IL10N;

/** @var array $_ */
/** @var IL10N $l */
/** @var Defaults $theme */
?>
<?php switch ($_['status']) {
	case 'deleted': ?>
		<p class="message warning guest-box">
			<?php
			p($l->t('Your account has been marked for deletion. You can now close this window.'));
		?>
		</p>
		<?php
		break;
	case 'not-found':
		?>
		<p class="message warning guest-box">
			<?php
			p($l->t('Account not found.'));
		?>
		</p>
		<?php break;
	case 'invalid-token': ?>
		<div class="warning guest-box">
			<p class="message">
				<strong>
					<?php
					p($l->t('The token provided was not found.'));
		?>
				</strong>
			</p>
			<p class="message">
				<?php
				p($l->t('Make sure the link opened is valid or that you are logged-in with the correct user.')); ?>
			</p>

			<p><a href="<?php p($_['rootURL']) ?>"><?php p($l->t('Back to %s', [$theme->getTitle()])); ?></a>
			</p>
		</div>
		<?php
		break;
	default: ?>
		<p class="message warning">
			<?php
			p($l->t('Unknown error.'));
		?>
		</p>
		<?php
		break;
} ?>
</p>
